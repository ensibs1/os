import threading
import time
from random import randint

class ConcurrentBuffer(threading.Thread):
    message = ["1664 – Quatre chiffres. Une bière.","Nike – Just do it.",]
    mutex = threading.BoundedSemaphore(1) # protect access to vars
    semWriter = threading.Semaphore(0) # protect write access to message
    semReader = threading.Semaphore(0) # protect read access to message

    # protected vars
    reader = 0
    acquireReader = 0 # queue of readers
    writer = 0
    acquireWriter = 0 # queue of writers

    def __init__(self, id: int, writer: bool):
        threading.Thread.__init__(self)
        # (appel au constructeur de la classe mère)
        self.id = id
        self.writer = writer

    def run(self):
        if self.writer:
            self.write()

        else:
            self.read(self.id)
        print("Fin du Thread " + str(self.id))

    def write(self):
        message = ["Actimel – Actimel renforce vos défenses naturelles.","Adidas – Impossible is nothing.","Airwaves – Mâchez. Respirez.","Red Bull – La boisson qui donne des aiiiles.","Sodebo – On se souvient surtout du goût."]
        global stop_threads
        while(stop_threads == False):
            i = randint(0,4)
            j = randint(0,len(message)-1)
            ConcurrentBuffer.mutex.acquire()  # requests access to protected vars
            if (ConcurrentBuffer.reader > 0 or ConcurrentBuffer.writer > 0 or ConcurrentBuffer.acquireWriter > 0): # priority for writers
                ConcurrentBuffer.acquireWriter += 1 # add himself to the queue of writers
                ConcurrentBuffer.mutex.release() # release access to protected vars
                ConcurrentBuffer.semWriter.acquire() # requests write access on the message
                ConcurrentBuffer.mutex.acquire()
                ConcurrentBuffer.acquireWriter -= 1 # remove himself of the queue
            ConcurrentBuffer.writer += 1 # announce that he's the writer
            ConcurrentBuffer.mutex.release()
            if (i < len(ConcurrentBuffer.message)):
                ConcurrentBuffer.message[i] = message[j]
                log = "Le rédacteur à remplacer le message " + str(i) + " par " + message[j]
                print(log + "\n")  # print the message
                f = open("log.txt", "a")
                f.write(log + "\nEtat liste de message : " + str(ConcurrentBuffer.message ) + "\n")
                f.close()
            else:
                ConcurrentBuffer.message.append(message[j])
                log = "Le rédacteur à ajouter un message à la position " + str(i) + " : " + message[j]
                print(log + "\n")  # print the message
                f = open("log.txt", "a")
                f.write(log + "\nEtat liste de message = " + str(ConcurrentBuffer.message ) + "\n")
                f.close()
            ConcurrentBuffer.mutex.acquire()
            ConcurrentBuffer.writer -= 1 # he's no longer the writer
            if (ConcurrentBuffer.acquireWriter > 0):
                ConcurrentBuffer.semWriter.release() # if writer(s) is/are waiting, release writer
            else:
                if (ConcurrentBuffer.acquireReader > 0):
                    for nb in range(ConcurrentBuffer.acquireReader):
                        ConcurrentBuffer.semReader.release() # if no writer is waiting but reader(s) is/are waiting, allow them to read
            ConcurrentBuffer.mutex.release()
            time.sleep(randint(1,5))

    def read(self, id):
        global stop_threads
        while(stop_threads == False):
            i = randint(0,len(ConcurrentBuffer.message)-1)
            ConcurrentBuffer.mutex.acquire()
            if (ConcurrentBuffer.writer > 0 or ConcurrentBuffer.acquireWriter > 0): # priority for writers
                ConcurrentBuffer.acquireReader += 1 # add himself to the queue of readers
                ConcurrentBuffer.mutex.release()
                ConcurrentBuffer.semReader.acquire() # requests read access on the message
                ConcurrentBuffer.mutex.acquire()
                ConcurrentBuffer.acquireReader -= 1 # remove himself of the queue
            ConcurrentBuffer.reader += 1 # announce that he's a reader
            ConcurrentBuffer.mutex.release()
            log = "Lecteur " + str(id) + " lit le message " + str(i) + " : " + ConcurrentBuffer.message[i]
            print(log + "\n") # print the message
            f = open("log.txt", "a")
            f.write(log + "\n")
            f.close()
            ConcurrentBuffer.mutex.acquire()
            ConcurrentBuffer.reader -= 1 # announce that he's no longer a reader
            if (ConcurrentBuffer.reader == 0 and ConcurrentBuffer.acquireWriter > 0):
                ConcurrentBuffer.semWriter.release() # if there is no reader and writers are waiting, release writer
            ConcurrentBuffer.mutex.release()
            time.sleep(randint(1,5)) # wait n seconds

if __name__ == '__main__':
    thread1 = ConcurrentBuffer(0, True)
    thread2 = ConcurrentBuffer(1, False)
    thread3 = ConcurrentBuffer(2, False)
    thread4 = ConcurrentBuffer(3, False)

    print("Ce programme affiche des slogans publicitaires toutes les 5 secondes. Un rédacteur intervient en tâche de fond à des instants aléatoires et modifie les messages ainsique leur durée de rafraîchissement.\n")
    print("Pour quitter le programme, entrez 'exit'\n")
    time.sleep(5)

    stop_threads = False
    f = open("log.txt", "w")
    f.write("log_file\n")
    f.close()

    thread2.start()
    thread3.start()
    thread4.start()
    time.sleep(5)
    thread1.start()

    while(True):
        user_input = input() # if there is no writer alive, listen for write requests
        if (user_input == "exit"):
            break # if input isn't an id but "exit" -> beak loop
    # stop threads
    stop_threads = True
    print("Stopping threads...")
    # wait until threads are stopped
    thread1.join()
    thread2.join()
    thread3.join()
    thread4.join()

