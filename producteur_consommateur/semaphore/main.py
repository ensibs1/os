import threading
import time

class ConcurrentBuffer(threading.Thread):
    stack = []
    bufferlen = 10
    mutex = threading.BoundedSemaphore(1) # protect access to stack
    places = threading.BoundedSemaphore(bufferlen) # don't exceed bufferlen
    items = threading.Semaphore(0) # protect access to items

    def __init__(self, item, producer):
        threading.Thread.__init__(self) # call on the parent class manufacturer
        # set attributes
        self.item = item
        self.producer = producer

    def run(self):
        for i in range(10):
            if self.producer:
                self.product(self.item)
                ConcurrentBuffer.mutex.acquire()
                print("Thread" + str(self.item) + ":")
                print(" - role: producer")
                print(" - stack = " + str(ConcurrentBuffer.stack) + "\n")
                ConcurrentBuffer.mutex.release()

            else:
                self.consume()
                ConcurrentBuffer.mutex.acquire()
                print("Thread" + str(self.item) + ":")
                print(" - role: consumer")
                print(" - stack = " + str(ConcurrentBuffer.stack) + "\n")
                ConcurrentBuffer.mutex.release()
            time.sleep(0.01)  # wait 100ms
        print("Fin du Thread " + str(self.item))

    def product(self, value):
        ConcurrentBuffer.places.acquire()    # try to find a place
        ConcurrentBuffer.mutex.acquire()     # check that nobody is writing in the stack
        ConcurrentBuffer.stack.append(value) # add an item
        ConcurrentBuffer.mutex.release()     # release the place of writer
        ConcurrentBuffer.items.release()     # informs that a new item may be consumed

    def consume(self):
        ConcurrentBuffer.items.acquire()  # try to consume an item
        ConcurrentBuffer.mutex.acquire()  # check that nobody is writing in the stack
        ConcurrentBuffer.stack.pop(0)     # consume the older item
        ConcurrentBuffer.mutex.release()  # release the place of writer
        ConcurrentBuffer.places.release() # informs that a new place is free

def runThreads(threads: list):
    for thread in threads:
        thread.start()

if __name__ == '__main__':
    thread1 = ConcurrentBuffer(1, True)
    thread2 = ConcurrentBuffer(2, True)
    thread3 = ConcurrentBuffer(3, False)
    thread4 = ConcurrentBuffer(4, False)

    runThreads([thread1, thread3, thread4, thread2])