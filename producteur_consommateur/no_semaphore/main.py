import threading
import time

class ConcurrentBuffer(threading.Thread):
    stack = []
    def __init__(self, item, producer):
        threading.Thread.__init__(self)
        # call on the parent class manufacturer
        # add attributes
        self.item = item
        self.producer = producer

    def run(self):
        for i in range(10):
            if self.producer:
                self.producer(self.item)
            else:
                self.consume()
            time.sleep(0.01)  # wait 100ms
        print("Fin du Thread " + str(self.item))

    def product(self, value):
        ConcurrentBuffer.stack.append(value) # add item
        print("Thread" + str(self.item) + ":")
        print(" - role: producer")
        print(" - stack = " + str(ConcurrentBuffer.stack) + "\n")

    def consume(self):
        ConcurrentBuffer.stack.pop(0) # consume item
        print("Thread" + str(self.item) + ":")
        print(" - role: consumer")
        print(" - stack = " + str(ConcurrentBuffer.stack) + "\n")



if __name__ == '__main__':
    thread1 = ConcurrentBuffer(1, True)
    thread2 = ConcurrentBuffer(2, True)
    thread3 = ConcurrentBuffer(3, False)
    thread4 = ConcurrentBuffer(4, False)

    thread1.start()
    thread3.start()
    thread4.start()
    thread2.start()