import threading
import time
from random import randint

class ConcurrentBuffer(threading.Thread):
    message = ["1664 – Quatre chiffres. Une bière.","Nike – Just do it.",]
    mutex = threading.BoundedSemaphore(1) # protect access to message
    semNbl = threading.BoundedSemaphore(1) # protect access to nbl
    nbl = 0

    def __init__(self, id: int, writer: bool):
        threading.Thread.__init__(self)
        # (appel au constructeur de la classe mère)
        self.id = id
        self.writer = writer

    def run(self):
        if self.writer:
            self.write()

        else:
            self.read(self.id)
        print("Fin du Thread " + str(self.id))

    def write(self):
        message = ["Actimel – Actimel renforce vos défenses naturelles.","Adidas – Impossible is nothing.","Airwaves – Mâchez. Respirez.","Red Bull – La boisson qui donne des aiiiles.","Sodebo – On se souvient surtout du goût."]
        global stop_threads
        while(stop_threads == False):
            i = randint(0,4)
            j = randint(0,len(message)-1)
            ConcurrentBuffer.mutex.acquire() # requests access to message
            if (i < len(ConcurrentBuffer.message)):
                ConcurrentBuffer.message[i] = message[j]
                log = "Le rédacteur à remplacer le message " + str(i) + " par " + message[j]
                print(log + "\n")  # print the message
                f = open("log.txt", "a")
                f.write(log + "\nEtat liste de message : " + str(ConcurrentBuffer.message ) + "\n")
                f.close()
            else:
                ConcurrentBuffer.message.append(message[j])
                log = "Le rédacteur à ajouter un message à la position " + str(i) + " : " + message[j]
                print(log + "\n")  # print the message
                f = open("log.txt", "a")
                f.write(log + "\nEtat liste de message = " + str(ConcurrentBuffer.message ) + "\n")
                f.close()
            ConcurrentBuffer.mutex.release() # release access to message
            time.sleep(randint(1,5))

    def read(self, id):
        global stop_threads
        while(stop_threads == False):
            i = randint(0,len(ConcurrentBuffer.message)-1)
            ConcurrentBuffer.semNbl.acquire() # requests access to nbl
            ConcurrentBuffer.nbl += 1
            if (ConcurrentBuffer.nbl == 1):
                ConcurrentBuffer.mutex.acquire() # if he's the first reader, lock access to message
            ConcurrentBuffer.semNbl.release() # release access to nbl
            log = "Lecteur " + str(id) + " lit le message " + str(i) + " : " + ConcurrentBuffer.message[i]
            print(log + "\n") # print the message
            f = open("log.txt", "a")
            f.write(log + "\n")
            f.close()
            ConcurrentBuffer.semNbl.acquire()
            ConcurrentBuffer.nbl -= 1
            if (ConcurrentBuffer.nbl == 0):
                ConcurrentBuffer.mutex.release() # if there is no more readers, release access to message ( may be for a writer )
            ConcurrentBuffer.semNbl.release()
            time.sleep(randint(1,5)) # wait n seconds

if __name__ == '__main__':
    thread1 = ConcurrentBuffer(0, True)
    thread2 = ConcurrentBuffer(1, False)
    thread3 = ConcurrentBuffer(2, False)
    thread4 = ConcurrentBuffer(3, False)

    print("Ce programme affiche des slogans publicitaires toutes les 5 secondes. Un rédacteur intervient en tâche de fond à des instants aléatoires et modifie les messages ainsique leur durée de rafraîchissement.\n")
    print("Pour quitter le programme, entrez 'exit'\n")
    time.sleep(5)

    stop_threads = False
    f = open("log.txt", "w")
    f.write("log_file\n")
    f.close()

    thread2.start()
    thread3.start()
    thread4.start()
    time.sleep(5)
    thread1.start()

    while(True):
        user_input = input() # if there is no writer alive, listen for write requests
        if (user_input == "exit"):
            break # if input isn't an id but "exit" -> beak loop
    # stop threads
    stop_threads = True
    print("Stopping threads...")
    # wait until threads are stopped
    thread1.join()
    thread2.join()
    thread3.join()
    thread4.join()